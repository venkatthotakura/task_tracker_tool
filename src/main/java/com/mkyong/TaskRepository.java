package com.mkyong;

import org.springframework.data.repository.CrudRepository;

import com.tasks.entity.TaskEntity;

public interface TaskRepository extends CrudRepository<TaskEntity, Long> {

//    List<Book> findByName(String name);

}
