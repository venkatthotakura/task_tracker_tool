package com.mkyong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = { "com.mkyong", "com.tasks" }) // force scan JPA entities
@EnableSwagger2
public class StartApplication /* implements CommandLineRunner */ {

	private static final Logger log = LoggerFactory.getLogger(StartApplication.class);

	@Autowired
	private TaskRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(StartApplication.class, args);
	}

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.mkyong")).build();
	}
	/*
	 * @Override public void run(String... args) {
	 * 
	 * log.info("StartApplication...");
	 * 
	 * repository.save(new Book("Java")); repository.save(new Book("Node"));
	 * repository.save(new Book("Python"));
	 * 
	 * System.out.println("\nfindAll()"); repository.findAll().forEach(x ->
	 * System.out.println(x));
	 * 
	 * System.out.println("\nfindById(1L)"); repository.findById(1l).ifPresent(x ->
	 * System.out.println(x));
	 * 
	 * System.out.println("\nfindByName('Node')");
	 * repository.findByName("Node").forEach(x -> System.out.println(x));
	 * 
	 * }
	 */

}