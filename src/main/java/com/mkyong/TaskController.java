package com.mkyong;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tasks.entity.Response;
import com.tasks.entity.TaskEntity;
import com.tasks.entity.UserEntity;
import com.tasks.enums.MessageCodes;
import com.tasks.enums.Messages;
import com.tasks.enums.TaskStatus;
import com.tasks.models.Task;
import com.tasks.models.TaskDetails;
import com.tasks.models.TaskUpdation;

@RestController
public class TaskController {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskLogRepository taskLogRepository;
	
	@Autowired
	AuthService authService;

	@GetMapping("/allTasks")
	public ResponseEntity<Object> tasks(@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		//Preparing response/output data
		response.setResults(taskRepository.findAll());
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASKS_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.tasksFetchedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/task/{id}")
	public ResponseEntity<Object> taskByID(@RequestParam (required = true ) Long id ,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		//Preparing response/output data
		List<TaskEntity> results = new ArrayList<>();
		results.add(taskRepository.findById(id).get());
		response.setResults(results);
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASK_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.taskFetchedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/addTask")
	public ResponseEntity<Object> addTask(@Valid @RequestBody Task taskCreation ,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		TaskEntity taskEntity = new TaskEntity();
		if(taskCreation.getTitle() != null) {
			if(isTitleExisted(taskCreation.getTitle())){
				response.setStatusCode(400);
				response.setResults(new ArrayList<>());
				response.setMessageCode(MessageCodes.TASK_TITLE_ALREADY_EXISTED);
				response.setMessage(Messages.taskTitleAlreadyExisted);
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}else {
				taskEntity.setTitle(taskCreation.getTitle());
			}
		}else {
			response.setStatusCode(400);
			response.setResults(new ArrayList<>());
			response.setMessageCode(MessageCodes.PROVIDE_TITLE_NAME);
			response.setMessage(Messages.provideTitleName);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(taskCreation.getUser() != null) {
			if(!isUserExisted(taskCreation.getUser())){
				response.setStatusCode(400);
				response.setResults(new ArrayList<>());
				response.setMessageCode(MessageCodes.USER_NOT_FOUND);
				response.setMessage(Messages.userNotFound);
				return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
			}else {
				taskEntity.setUser(taskCreation.getUser());
			}
		}
		taskEntity.setStatus(TaskStatus.NOT_STARTED.toString());
		taskEntity.setHoursEstimation(taskCreation.getHoursEstimation());
		taskEntity.setHoursRemaining(taskCreation.getHoursEstimation());
		taskEntity.setDescription(taskCreation.getDescription());
		taskEntity.setHoursLogged(0);
		
		taskRepository.save(taskEntity);
		//Preparing response/output data
		List<TaskEntity> results = new ArrayList<>();
		results.add(taskEntity);
		response.setResults(results);
		response.setStatusCode(201);
		response.setMessageCode(MessageCodes.TASK_CREATED_SUCCESSFULLY);
		response.setMessage(Messages.taskCreatedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@PutMapping("/updateTask")
	public ResponseEntity<Object> updateTask(@Valid @RequestBody TaskUpdation taskUpdation ,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		TaskEntity taskEntity ;
		if (taskUpdation.getId() != null) {
			if(null != taskRepository.findById(taskUpdation.getId()) && !taskRepository.findById(taskUpdation.getId()).isEmpty()) {
				taskEntity = taskRepository.findById(taskUpdation.getId()).get();
			}else {
				response.setResults(new ArrayList<>());
				response.setStatusCode(400);
				response.setMessageCode(MessageCodes.INVALID_TASK_ID);
				response.setMessage(Messages.invalidtaskID);
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			
		}else {
			response.setResults(new ArrayList<>());
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.TASK_ID_NOT_EXISTED);
			response.setMessage(Messages.taskIdManadatary);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(taskUpdation.getTitle() != null) {
			if(!taskEntity.getTitle().equalsIgnoreCase(taskUpdation.getTitle())) {
				if(isTitleExisted(taskUpdation.getTitle())){
					response.setResults(new ArrayList<>());
					response.setStatusCode(400);
					response.setMessageCode(MessageCodes.TASK_TITLE_ALREADY_EXISTED);
					response.setMessage(Messages.taskTitleAlreadyExisted);
					return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
				}else {
					taskEntity.setTitle(taskUpdation.getTitle());
				}
			}
		}

		if(taskUpdation.getUser() != null) {
			if(!isUserExisted(taskUpdation.getUser())) {
				response.setResults(new ArrayList<>());
				response.setStatusCode(400);
				response.setMessageCode(MessageCodes.USER_NOT_FOUND);
				response.setMessage(Messages.userNotFound);
				return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
			}else {
				taskEntity.setUser(taskUpdation.getUser());
			}
		}
		
		if(taskUpdation.getStatus() != null) {
			if(!isTaskStatusValid(taskUpdation.getStatus())) {
				response.setResults(new ArrayList<>());
				response.setStatusCode(400);
				response.setMessageCode(MessageCodes.INVALID_TASK_STATUS);
				response.setMessage(Messages.taskStatuaInvalid);
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}else {
				taskEntity.setStatus(taskUpdation.getStatus());
			}
		}
		if(taskUpdation.getHoursEstimation() == 0) {
			taskEntity.setHoursEstimation(taskUpdation.getHoursEstimation());
		}
		if(null != taskUpdation.getDescription() && !taskUpdation.getDescription().isEmpty()) {
			taskEntity.setDescription(taskUpdation.getDescription());
		}
		
		taskRepository.save(taskEntity);
		
		//Preparing response/output data
		List<TaskEntity> results = new ArrayList<>();
		results.add(taskEntity);
		response.setResults(results);
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASK_UPDATED_SUCCESSFULLY);
		response.setMessage(Messages.taskUpdatedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PutMapping("/updateTaskStatus/{id}/{status}")
	public ResponseEntity<Object> updateTaskStatus(@RequestParam (required = true ) Long id ,@RequestParam (required = true ) TaskStatus status,
			@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
        TaskEntity taskEntity ;
	    if(null != taskRepository.findById(id) && !taskRepository.findById(id).isEmpty()) {
			taskEntity = taskRepository.findById(id).get();
		}else {
			response.setResults(new ArrayList<>());
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.INVALID_TASK_ID);
			response.setMessage(Messages.taskIdNotExisted);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	    
	    taskEntity.setStatus(status.toString());
	    
	    taskRepository.save(taskEntity);
		//Preparing response/output data
		List<TaskEntity> results = new ArrayList<>();
		results.add(taskEntity);
		response.setResults(results);
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASK_UPDATED_SUCCESSFULLY);
		response.setMessage(Messages.taskUpdatedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PutMapping("/setTaskPlanToWorkToday/{id}")
	public ResponseEntity<Object> setTaskPlanToWorkToday(@RequestParam (required = true ) Long id ,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
        TaskEntity taskEntity ;
	    if(null != taskRepository.findById(id) && !taskRepository.findById(id).isEmpty()) {
			taskEntity = taskRepository.findById(id).get();
		}else {
			response.setResults(new ArrayList<>());
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.INVALID_TASK_ID);
			response.setMessage(Messages.taskIdNotExisted);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	    
	    taskEntity.setPlanToWorkToday(true);
	    
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		Date date = new Date();  
	    taskEntity.setPlanToWorkTodayLoggedDate(formatter.format(date));
	    
	    taskRepository.save(taskEntity);
		//Preparing response/output data
		List<TaskEntity> results = new ArrayList<>();
		results.add(taskEntity);
		response.setResults(results);
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASK_UPDATED_SUCCESSFULLY);
		response.setMessage(Messages.taskUpdatedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/allTasksWithLoggingDetails")
	public ResponseEntity<Object> allTaskswithLoggingDetails(@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		List<TaskDetails> data = new ArrayList<>();
		List<TaskEntity> tasks = (List<TaskEntity>) taskRepository.findAll();
		for(TaskEntity task : tasks) {
			TaskDetails details = new TaskDetails();
			details.setId(task.getId());
			details.setTitle(task.getTitle());
			details.setDescription(task.getDescription());
			details.setStatus(task.getStatus());
			details.setUser(task.getUser());
			details.setHoursEstimation(task.getHoursEstimation());
			details.setHoursRemaining(task.getHoursRemaining());
			details.setHoursLogged(task.getHoursLogged());
			details.setLoggingDetails(taskLogRepository.findByTaskId(Integer.parseInt(String.valueOf(task.getId()))));
			data.add(details);
		}
		
		//Preparing response/output data
		response.setResults(data);
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASKS_WITH_LOG_DETAILS_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.tasksWithLogDetailsFetchedSuccessFully);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	private boolean isTaskStatusValid(String statusName) {
		boolean flag = false;
		TaskStatus[] statuses = TaskStatus.values();
		for (TaskStatus status : statuses) {
			if (status.toString().equalsIgnoreCase(statusName)) {
				flag = true;
			}
		}
		return flag;
	}

	private boolean isTitleExisted(String title) {
		boolean flag = false;
		List<TaskEntity> tasks = (List<TaskEntity>) taskRepository.findAll();
		for (TaskEntity task : tasks) {
			if (task.getTitle().equalsIgnoreCase(title)) {
				flag = true;
			}
		}
		return flag;
	}
	
	private boolean isUserExisted(String userName) {
		boolean flag = false;
		List<UserEntity> users = (List<UserEntity>) userRepository.findAll();
		for (UserEntity user : users) {
			if (user.getName().equalsIgnoreCase(userName)) {
				flag = true;
			}
		}
		return flag;
	}
}
