package com.mkyong;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tasks.entity.TaskEntity;
import com.tasks.entity.UserEntity;

@Component
public class AppSession {

	@Autowired
	private static TaskRepository taskRepository;

	@Autowired
	private static UserRepository userRepository;

	@Autowired
	private TaskLogRepository taskLogRepository;

	public static List<Long> taskIds;

	public static List<String> userNames;
	
	public static void resetUsers() {
		userNames = new ArrayList<>();
	}
	
	public static void setUserNames(List<String> userNamesList) {
		userNames =userNamesList ;
	}

	public static void updateSessionData() {

		List<UserEntity> users = (List<UserEntity>) userRepository.findAll();
		userNames = new ArrayList<>();
		for (UserEntity user : users) {
			userNames.add(user.getName());
		}

		List<TaskEntity> tasks = (List<TaskEntity>) taskRepository.findAll();
		taskIds = new ArrayList<>();
		for (TaskEntity task : tasks) {
			taskIds.add(task.getId());
		}

	}

}
