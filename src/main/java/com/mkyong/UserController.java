package com.mkyong;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tasks.entity.Response;
import com.tasks.entity.UserEntity;
import com.tasks.enums.MessageCodes;
import com.tasks.enums.Messages;
import com.tasks.models.User;

@RestController
public class UserController {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	AuthService authService;

	@GetMapping("/users")
	public ResponseEntity<Object> users() 
	{
		Response response = new Response();
		
		//Preparing response/output data
		response.setResults(repository.findAll());
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.USERS_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.usersFetchedSuccessfully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/addUser")
	public ResponseEntity<Object> addUser(
			@Valid @RequestBody User user) 
	{
		Response response = new Response();
		
		UserEntity userEntity = new UserEntity();
		if(isUserExisted(user.getName())) {
			response.setResults(new ArrayList<>());
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.USER_ALREADY_EXISTED);
			response.setMessage(Messages.userAlreadyExisted);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}else {
			userEntity.setName(user.getName());
		}
		userEntity.setPassword(user.getPassword());
		
		repository.save(userEntity);
		
		//Preparing response/output data
		List<UserEntity> results = new ArrayList<>();
		results.add(userEntity);
		response.setResults(results);
		response.setStatusCode(201);
		response.setMessageCode(MessageCodes.USER_CREATED_SUCCESSFULLY);
		response.setMessage(Messages.userCreatedSuccessfully);
		
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	@PostMapping("/login")
	public ResponseEntity<Object> login(@Valid @RequestBody User user) 
	{
		Response response = new Response();
		
		if(isUserExisted(user.getName())) {
			UserEntity userEntity = repository.findByName(user.getName());
			if(userEntity.getPassword().contentEquals(user.getPassword())) {
				userEntity.setAccessToken(Utility.generateToken());
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				userEntity.setAccessTokenValidity(formatter.format(Utility.addHoursToJavaUtilDate(new Date(),6)));
				
				repository.save(userEntity);
				
				//Preparing response/output data
				List<UserEntity> results = new ArrayList<>();
				results.add(userEntity);
				response.setResults(results);
				response.setStatusCode(200);
				response.setMessageCode(MessageCodes.USER_LOGGED_IN_SUCCESSFULLY);
				response.setMessage(Messages.userLoggedInSuccessfully);
				
				return new ResponseEntity<>(response, HttpStatus.OK);
			}else {
				//Preparing response/output data
				List<UserEntity> results = new ArrayList<>();
				response.setResults(results);
				response.setStatusCode(400);
				response.setMessageCode(MessageCodes.INCORRECT_PASSWORD);
				response.setMessage(Messages.incorrectPassword);
				
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
		}else {
			
			//Preparing response/output data
			List<UserEntity> results = new ArrayList<>();
			response.setResults(results);
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.INVALID_USER_NAME);
			response.setMessage(Messages.invalidUserName);
			
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/deleteUser/{userName}")
	public ResponseEntity<Object> deleteUser(
			@RequestParam(required = true) String userName)
	{
		Response response = new Response();
		UserEntity userEntity = repository.findByName(userName);
		if(null != userEntity) {
			
			repository.delete(userEntity);
			
			//Preparing response/output data
			List<UserEntity> results = new ArrayList<>();
			results.add(userEntity);
			response.setResults(results);
			response.setStatusCode(200);
			response.setMessageCode(MessageCodes.USER_DELETED_SUCCESSFULLY);
			response.setMessage(Messages.userDeletedSuccessfully);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}else {
			
			//Preparing response/output data
			List<UserEntity> results = new ArrayList<>();
			response.setResults(results);
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.INVALID_USER_NAME);
			response.setMessage(Messages.invalidUserName);
			
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

	private boolean isUserExisted(String userName) {
		boolean flag = false;
		List<UserEntity> users = (List<UserEntity>) repository.findAll();
		for (UserEntity user : users) {
			if (user.getName().equalsIgnoreCase(userName)) {
				flag = true;
			}
		}
		return flag;
	}
	
}
