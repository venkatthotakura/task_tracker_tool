package com.mkyong;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tasks.entity.Response;
import com.tasks.entity.TaskEntity;
import com.tasks.entity.TaskLogEntity;
import com.tasks.entity.UserEntity;
import com.tasks.enums.MessageCodes;
import com.tasks.enums.Messages;
import com.tasks.models.TaskLog;

@RestController
public class TaskLogController {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskLogRepository taskLogRepository;
	
	@Autowired
	AuthService authService;
	
	@GetMapping("/taskLogs/{taskId}")
	public ResponseEntity<Object> taskLogsByTaskId(@RequestParam (required = true ) int taskId,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		//Preparing response/output data
		response.setResults(taskLogRepository.findByTaskId(taskId));
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASKS_LOGS_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.tasksLogsFetchedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/taskLogs/{user}")
	public ResponseEntity<Object> taskLogsByUser(@RequestParam (required = true ) String user ,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}	
		
		//Preparing response/output data
		response.setResults(taskLogRepository.findByUser(user));
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASKS_LOGS_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.tasksLogsFetchedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/addTaskLog")
	public ResponseEntity<Object> addTaskLog(@Valid @RequestBody TaskLog taskLog,@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		TaskLogEntity taskLogEntity = new TaskLogEntity();
		
		if(isTaskIdExisted(taskLog.getTaskId())) {
			taskLogEntity.setTaskId(taskLog.getTaskId());
		}else {
			response.setResults(new ArrayList<>());
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.TASK_ID_NOT_EXISTED);
			response.setMessage(Messages.taskIdNotExisted);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(isUserExisted(taskLog.getUser())) {
			taskLogEntity.setUser(taskLog.getUser());
		}else {
			response.setResults(new ArrayList<>());
			response.setStatusCode(400);
			response.setMessageCode(MessageCodes.USER_NOT_FOUND);
			response.setMessage(Messages.userNotFound);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		taskLogEntity.setLogHours(taskLog.getLogHours());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		Date date = new Date();  
		taskLogEntity.setLoggedDate(formatter.format(date));
		
		taskLogRepository.save(taskLogEntity);
		//Preparing response/output data
		List<TaskLogEntity> results = new ArrayList<>();
		results.add(taskLogEntity);
		response.setResults(results);
		response.setStatusCode(201);
		response.setMessageCode(MessageCodes.TASK_LOG_SAVED_SUCCESSFULLY);
		response.setMessage(Messages.taskLogSavedSuccessFully);
		
		TaskEntity taskEntity = taskRepository.findById(Long.valueOf(String.valueOf(taskLog.getTaskId()))).get();
		taskEntity.setHoursLogged(taskEntity.getHoursLogged()+taskLog.getLogHours());
		if(taskEntity.getHoursRemaining()-taskLog.getLogHours() < 0) {
			taskEntity.setHoursRemaining(0);
		}else {
			taskEntity.setHoursRemaining(taskEntity.getHoursRemaining()-taskLog.getLogHours());
		}
		taskRepository.save(taskEntity);
		
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	@GetMapping("/allTaskLogs")
	public ResponseEntity<Object> allTaskLogs(@RequestHeader("accessToken") String accessToken) 
	{
		//Authentication
		Response response = new Response();
		if(!authService.authenticate(response,accessToken)) {
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		
		//Preparing response/output data
		response.setResults(taskLogRepository.findAll());
		response.setStatusCode(200);
		response.setMessageCode(MessageCodes.TASKS_LOGS_FETCHED_SUCCESSFULLY);
		response.setMessage(Messages.tasksLogsFetchedSuccessFully);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	private boolean isUserExisted(String userName) {
		boolean flag = false;
		List<UserEntity> users = (List<UserEntity>) userRepository.findAll();
		for (UserEntity user : users) {
			if (user.getName().equalsIgnoreCase(userName)) {
				flag = true;
			}
		}
		return flag;
	}
	
	private boolean isTaskIdExisted(int taskId) {
		boolean flag = false;
		List<TaskEntity> tasks = (List<TaskEntity>) taskRepository.findAll();
		for (TaskEntity task : tasks) {
			if (task.getId() == taskId) {
				flag = true;
			}
		}
		return flag;
	}
}
