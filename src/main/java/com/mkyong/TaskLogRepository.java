package com.mkyong;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.tasks.entity.TaskLogEntity;

public interface TaskLogRepository extends CrudRepository<TaskLogEntity, Long> {
	
	List<TaskLogEntity> findByTaskId(int taskId);
	
	List<TaskLogEntity> findByUser(String user);

}
