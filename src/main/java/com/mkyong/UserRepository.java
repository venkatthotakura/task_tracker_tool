package com.mkyong;

import org.springframework.data.repository.CrudRepository;

import com.tasks.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

//    List<Book> findByName(String name);
	
	UserEntity findByName(String name);

}
