package com.mkyong;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tasks.entity.Response;
import com.tasks.entity.UserEntity;
import com.tasks.enums.MessageCodes;
import com.tasks.enums.Messages;

@Service
public class AuthService {
	
	@Autowired
	private UserRepository userRepository;
	
	public boolean authenticate(Response response , String accessToken) {
		if(isTokenValid(accessToken)) {
			if(!isTokenNotExpired()) {
				response.setStatusCode(401);
				response.setMessageCode(MessageCodes.ACCESS_TOKEN_EXPIRED);
				response.setMessage(Messages.accessTokenExpired);
				response.setResults(new ArrayList<>());
				return false;
			}
		}else {
			response.setStatusCode(401);
			response.setMessageCode(MessageCodes.INVALID_ACCESS_TOKEN);
			response.setMessage(Messages.invalidAccessToken);
			response.setResults(new ArrayList<>());
			return false;
		}
		return true;
	}
	
	public boolean isTokenValid(String token) {
		List<UserEntity> users = (List<UserEntity>) userRepository.findAll();
		for(UserEntity user : users) {
			if(null != user.getAccessToken() && user.getAccessToken().equalsIgnoreCase(token)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isTokenNotExpired() {
		List<UserEntity> users = (List<UserEntity>) userRepository.findAll();
		for(UserEntity user : users) {
			if(Utility.tokenValidyCheck(user.getAccessTokenValidity())) {
				return true;
			}
		}
		return false;
	}
}
