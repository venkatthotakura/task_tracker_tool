package com.mkyong;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Utility {

	public static String generateToken() {
		StringBuilder token = new StringBuilder();
		long currentTimeInMilisecond = Instant.now().toEpochMilli();
		token.append(currentTimeInMilisecond).append("-").append(UUID.randomUUID().toString()).toString();
		return token.toString();
	}

	public static Date addHoursToJavaUtilDate(Date date, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, hours);
		return calendar.getTime();
	}

	public static boolean tokenValidyCheck(String tokenValidityDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Date date = new Date(); 
			Date d1 = formatter.parse(formatter.format(date));
			Date d2 = formatter.parse(tokenValidityDate);
			if (d1.compareTo(d2) < 0) {
				return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

}
