package com.tasks.models;

import javax.validation.constraints.NotEmpty;

public class User {

	@NotEmpty(message = "User name is required.")
	String name;

	@NotEmpty(message = "Password is required.")
	String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
