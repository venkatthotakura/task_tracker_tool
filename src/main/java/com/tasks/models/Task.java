package com.tasks.models;

import javax.validation.constraints.NotEmpty;

public class Task {

	String title;

	private String user;

//	private String status;

	private int hoursEstimation;

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	/*
	 * public String getStatus() { return status; }
	 * 
	 * public void setStatus(String status) { this.status = status; }
	 */

	public int getHoursEstimation() {
		return hoursEstimation;
	}

	public void setHoursEstimation(int hoursEstimation) {
		this.hoursEstimation = hoursEstimation;
	}

}
