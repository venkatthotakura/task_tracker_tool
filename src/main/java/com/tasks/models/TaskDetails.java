package com.tasks.models;

import java.util.List;

import com.tasks.entity.TaskLogEntity;

public class TaskDetails {

	private Long id;

	private String title;

	private String description;

	private String user;

	private String status;

	private int hoursEstimation;

	private int hoursRemaining;

	private int hoursLogged;

	List<TaskLogEntity> loggingDetails;

	public List<TaskLogEntity> getLoggingDetails() {
		return loggingDetails;
	}

	public void setLoggingDetails(List<TaskLogEntity> loggingDetails) {
		this.loggingDetails = loggingDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getHoursEstimation() {
		return hoursEstimation;
	}

	public void setHoursEstimation(int hoursEstimation) {
		this.hoursEstimation = hoursEstimation;
	}

	public int getHoursRemaining() {
		return hoursRemaining;
	}

	public void setHoursRemaining(int hoursRemaining) {
		this.hoursRemaining = hoursRemaining;
	}

	public int getHoursLogged() {
		return hoursLogged;
	}

	public void setHoursLogged(int hoursLogged) {
		this.hoursLogged = hoursLogged;
	}

}
