package com.tasks.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TaskLog {

	@NotNull(message = "Task id is required.")
	private int taskId;

	@NotEmpty(message = "User is required.")
	private String user;

	@NotNull(message = "Log hours( In Hoours ) is required.")
	private int logHours;

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getLogHours() {
		return logHours;
	}

	public void setLogHours(int logHours) {
		this.logHours = logHours;
	}

}
