package com.tasks.enums;

public enum TaskStatus {
	NOT_STARTED, IN_PROGRESS, ON_HOLD, IN_QA, COMPLETED
}
