package com.tasks.enums;

public class Messages {

	public static String taskCreated = "Task created successfully.";
	public static String invalidAccessToken = "Invalid accesstoken.";
	public static String accessTokenExpired = "Access token was expired .Please login again to get new token.";
	public static String taskFetchedSuccessFully = "Task fetched succssfully.";
	public static String tasksFetchedSuccessFully = "Tasks fetched succssfully.";
	public static String taskCreatedSuccessFully = "Task created succssfully.";
	public static String taskUpdatedSuccessFully = "Task updated succssfully.";
	public static String tasksWithLogDetailsFetchedSuccessFully = "Tasks with log details fetched succssfully.";
	public static String tasksLogsFetchedSuccessFully = "Tasks logs fetched succssfully.";
	public static String taskLogSavedSuccessFully = "Task log saved succssfully.";
	public static String userCreatedSuccessfully = "User created succssfully.";
	public static String userDeletedSuccessfully = "User deleted succssfully.";
	public static String userAlreadyExisted = "User alreday existed.";
	public static String invalidUserName = "Please provide a valid user";
	public static String incorrectPassword = "Please provide correct password.";
	public static String userLoggedInSuccessfully = "User Logged-in successfully.";
	public static String usersFetchedSuccessfully = "Users fetched succssfully.";
	public static String taskTitleAlreadyExisted = "Title already existed, please provide new title.";
	public static String userNotFound = "User not found , please provide existed user name or create a new username to use";
	public static String provideTitleName = "Please provide a valid title name";
	public static String taskIdNotExisted = "Task ID not existed , please provie a valid task id.";
	public static String taskStatuaInvalid = "Task Satus is invalid, please provide a valid status";
	public static String taskIdManadatary = "Task ID is mandatory to update the task.";
	public static String invalidtaskID = "Please provide a valid Task ID";
}
