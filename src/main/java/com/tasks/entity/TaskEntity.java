package com.tasks.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "task")
public class TaskEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String title;

	private String description;

	private String user;

	private String status;

	private int hoursEstimation;

	private int hoursRemaining;

	private int hoursLogged;
	
	private boolean planToWorkToday;
	
	private String planToWorkTodayLoggedDate;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getHoursEstimation() {
		return hoursEstimation;
	}

	public void setHoursEstimation(int hoursEstimation) {
		this.hoursEstimation = hoursEstimation;
	}

	public int getHoursRemaining() {
		return hoursRemaining;
	}

	public void setHoursRemaining(int hoursRemaining) {
		this.hoursRemaining = hoursRemaining;
	}

	public int getHoursLogged() {
		return hoursLogged;
	}

	public void setHoursLogged(int hoursLogged) {
		this.hoursLogged = hoursLogged;
	}

	public boolean isPlanToWorkToday() {
		return planToWorkToday;
	}

	public void setPlanToWorkToday(boolean planToWorkToday) {
		this.planToWorkToday = planToWorkToday;
	}

	public String getPlanToWorkTodayLoggedDate() {
		return planToWorkTodayLoggedDate;
	}

	public void setPlanToWorkTodayLoggedDate(String planToWorkTodayLoggedDate) {
		this.planToWorkTodayLoggedDate = planToWorkTodayLoggedDate;
	}
}
